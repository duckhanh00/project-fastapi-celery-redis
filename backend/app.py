from fastapi import FastAPI, Request
from celery import Celery
from redis_broker.pub import publish
from typing import List

app = FastAPI()
celery = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')


@app.post('/send_message')
def send_message(data):
	res = publish(data)
	
	return data


# @app.post('/celery_start_task')
# def celery_start_task():
# 	print("start")
# 	res_add = send_list.delay()
	
# 	return "oke"

# @app.post('/celery_task_status/<task_id>')
# def get_status(task_id):
#     status = celery.AsyncResult(task_id, app=celery)
#     print("Invoking Method ")
#     return "Status of the Task " + str(status.state)


# @app.post('/celery_task_result/<task_id>')
# def task_result(task_id):
#     result = celery.AsyncResult(task_id)
#     return "Result of the Task " + str(result)
	






