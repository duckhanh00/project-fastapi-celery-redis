import redis
import time
from config import RedisConfig

r = redis.Redis(host=RedisConfig.HOST, port=RedisConfig.PORT)
pub = r.pubsub()

def publish(data):
    r.publish('info', str(data))

