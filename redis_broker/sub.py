import redis
import time
from config import RedisConfig

r = redis.Redis(host=RedisConfig.HOST, port=RedisConfig.PORT)
sub = r.pubsub()
sub.subscribe("info")