from re import L
from typing import List

def check_list_1(list: List):
    sum = 0
    for i in list:
        i = int(i)
        sum += i
    if sum == 6:
        return True
    return False

def check_list_2(list: List):
    sum = 0
    for i in list:
        i = int(i)
        sum += i
    if sum == 15:
        return True
    return False