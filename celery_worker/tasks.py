import time
from celery import Celery
from typing import List
from celery_worker.conditions import check_list_1, check_list_2
from config import RedisConfig
from termcolor import colored

celery = Celery('tasks', broker=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}/0', backend='redis://localhost:6379/1')

@celery.task()
def task_1(list: List):
    if check_list_1(list):
        print(colored('LIST FROM 1 - 3: COMPLETE', 'green'))
        return list
    return None

@celery.task()
def task_2(list: List):
    if check_list_2(list):
        print(colored('LIST FROM 6 - 8: COMPLETE', 'green'))
        return list
    return None

