import redis
from config import RedisConfig

r = redis.Redis(host=RedisConfig.HOST, port=RedisConfig.PORT)
pub = r.pubsub()

def publish(data):
    r.publish('info', str(data))

for i in range(20):
    publish(i)