# Set up environment
python3 -m venv .venv
source .venv/bin/activate
pip install -U celery[msgpack,redis]
pip install -r requirement.txt
# RUN Redis
sudo docker-compose up -d
# RUN FastAPI
uvicorn backend.app:app --reload
# RUN Celery
celery -A celery_worker.tasks worker --loglevel=INFO
# RUN mainprocess
python3 mainprocess.py
# RUN send message
python3 test.py
or
API send_message


