from re import L
import redis
import time
from config import RedisConfig
from celery_worker.tasks import task_1, task_2
from termcolor import colored

async def main():
    r = redis.Redis(host=RedisConfig.HOST, port=RedisConfig.PORT)
    sub = r.pubsub()
    sub.subscribe("info")
    list_task1 = []
    list_task2 = []
    for item in sub.listen():
        if item['type'] == 'message':
            message = item['data'].decode("utf-8")
            print(message)
            message = int(message)

            if message in range(4) and message not in list_task1:
                list_task1.append(message)
                print("list_task1: ", list_task1)
                if task_1(list_task1):
                    list_task1.clear()

            elif message in range(4,7) and message not in list_task2:
                list_task2.append(message)
                print("list_task2: ", list_task2)
                if task_2(list_task2):
                    list_task2.clear()
                    
            else: 
                print(colored("NOT IN TASK", 'red'))
                continue

if __name__ == "__main__":
	import asyncio
	loop = asyncio.get_event_loop()
	loop.run_until_complete(main())